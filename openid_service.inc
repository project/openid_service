<?php

/**
 * @file
 *  Openid login services module.
 */


/**
 * Login a user
 *
 * @param $openid
 *   String. The openid.
 */
function openid_service_login($openids) {
  global $user;

  if ($user->uid) {
    // user is already logged in
    return services_error(t('Already logged in as !user.', array('!user' => $user->name)));
  }

  if (! is_array($openids)) {
    $openids = array($openids);
  }

  foreach ($openids as $openid) {
    // add 'http://' if openid supplied did not include it
    if (stristr($openid, '://') === FALSE) {
      $openid = 'http://'. $openid; //todo: SSL check
    }
    if (substr_count($openid, '/') < 3) {
      $openid .= '/';
    }

    // Find the matching uid for the openid supplied
    $sql = "SELECT uid FROM {authmap} WHERE authname LIKE '%s' OR authname LIKE '%s/'";
    $uid = db_result(db_query($sql, $openid));
    $account = user_load(array('uid' => $uid, 'status' => 1));
    if ($account->uid > 0) {
      $user = $account;
      $form_values['name'] = $user->name;
      user_authenticate_finalize($form_values);
    } else {
      continue;
    }

    if ($user->uid > 0) {
      return session_id();
    }
    session_destroy();
  }
  return services_error(t('No user with that OpenID.'));
}
